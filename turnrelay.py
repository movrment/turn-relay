#coding: utf-8
"""Basic implementation of a STUN client.
STUN lets you discover your publicly-visible IP address, as seen by a
particular server or servers.  Some NATs will assign a different address for
each outgoing connection, so this client lets you query multiple servers and
discover possibly multiple ports and IP addresses.
This is usable as a standalone program or a python library module.

Author: mov
Original idea -> https://www.rtcsec.com/2020/04/01-slack-webrtc-turn-compromise/
"""
import os
import select
import socket
import struct
import sys
import time
import argparse
import binascii
import hashlib
import hmac

DEBUG = False
NET_MSG_SIZE = 1024
PORT = 3478
MAGIC_COOKIE = 0x2112a442  # defined by the STUN standard, not a secret
# STUN request types
MSG_BIND_REQUEST = 0x0001
MSG_ALLOCATE_REQUEST=0x0003
MSG_CREATEPERMISSION_REQUEST=0x0008
MSG_CONNECTION_REQUEST=0x000a
MSG_CONNECTIONBIND_REQUEST=0x000b
MSG_BIND_INDICATION = 0x0011
MSG_SEND_INDICATOR = 0x0016
MSG_DATA_INDICATOR = 0x0017
MSG_BIND_SUCCESS = 0x0101
MSG_ALLOCATE_SUCCESS=0x0103
MSG_CREATEPERMISSION_SUCCESS=0x0108
MSG_CONNECTION_SUCCESS=0x010a
MSG_CONNECTIONBIND_SUCCESS=0x010b
MSG_BIND_ERROR = 0x0111
MSG_ALLOCATE_ERROR = 0x0113
# STUN tag:value attributes
ATTR_MASK = 0x07ff
ATTR_MAPPED_ADDRESS = 0x0001
ATTR_USERNAME = 0x0006
ATTR_MESSAGE_INTEGRITY = 0x0008
ATTR_ERROR_CODE = 0x0009
ATTR_UNKNOWN_ATTRIBUTES = 0x000a
ATTR_LIFETIME = 0x000d
ATTR_XOR_PEER_ADDRESS = 0x0012
ATTR_DATA = 0x0013
ATTR_REALM = 0x0014
ATTR_NONCE = 0x0015
ATTR_XOR_RELAYED_ADDRESS= 0x0016
ATTR_REQUESTED_TRANSPORT = 0x0019
ATTR_XOR_MAPPED_ADDRESS = 0x0020
ATTR_CONNECTION_ID = 0x002a
ATTR_SOFTWARE = 0x8022
ATTR_ALTERNATE_SERVER = 0x8023
ATTR_FINGERPRINT = 0x8028
# List of Google-provided STUN servers.
GOOGLE_SERVER_LIST = [
    'stun.l.google.com:19302',
    'stun1.l.google.com:19302',
    'stun2.l.google.com:19302',
    'stun3.l.google.com:19302',
    'stun4.l.google.com:19302',
]
# Some non-Google-provided STUN servers, not used by default.
# copied from:
# http://code.google.com/p/natvpn/source/browse/trunk/stun_server_list
EXTRA_SERVER_LIST = [
    'stun01.sipphone.com',
    'stun.ekiga.net',
    'stun.fwdnet.net',
    'stun.ideasip.com',
    'stun.iptel.org',
    'stun.rixtelecom.se',
    'stun.schlund.de',
    'stunserver.org',
    'stun.softjoys.com',
    'stun.voiparound.com',
    'stun.voipbuster.com',
    'stun.voipstunt.com',
    'stun.voxgratia.org',
    'stun.xten.com',
]

class Error(Exception):
  pass
class ParseError(Error):
  pass
class TimeoutError(Error):
  pass
class DnsError(Error):
  pass
try:
  import monotime  # pylint: disable=unused-import,g-import-not-at-top
except ImportError:
  pass
try:
  gettime = time.monotonic
except AttributeError:
  gettime = time.time
def Log(s, *args):
  sys.stdout.flush()
  s += '\n'
  if args:
    sys.stderr.write(s % args)
  else:
    sys.stderr.write(s)
  sys.stderr.flush()
def _EncodeAttr(attrtype, value):
  value = (value)
  pkt = struct.pack('!HH', attrtype, len(value)) + value
  while (len(pkt) % 4) != 0:  # 32-bit alignment
    pkt += b'\0'
  return pkt
def _DecodeAddress(value):
  family, port = struct.unpack('!HH', value[:4])
  addr = value[4:]
  if family == 0x01:  # IPv4
    return socket.inet_ntop(socket.AF_INET, addr), port
  elif family == 0x02:  # IPv6
    return socket.inet_ntop(socket.AF_INET6, addr), port
  else:
    raise ValueError('invalid family %d: expected 1=IPv4 or 2=IPv6' % family)
def _XorChars(s, keystr):
  for a, b in zip(s, keystr):
    yield bytes([a ^ b])

def _Xor(s, keystr):
  return b''.join(_XorChars(s, keystr))
def _DecodeAttrs(text, transaction_id):
  while text:
    attrtype, length = struct.unpack('!HH', text[:4])
    value = text[4:4+length]
    length += (4 - (length % 4)) % 4  # 32-bit alignment
    text = text[4+length:]
    if (attrtype & ATTR_MASK) == (ATTR_XOR_MAPPED_ADDRESS & ATTR_MASK):
      # technically I think XOR_MAPPED_ADDRESS *without* the 0x8000 flag is
      # a bug based on my reading of rfc5389, but some servers produce it
      # anyhow, so fine.
      key = struct.pack('!I', MAGIC_COOKIE) + transaction_id
      value = value[0:2] + _Xor(value[2:4], key) + _Xor(value[4:], key)
      yield ATTR_MAPPED_ADDRESS, _DecodeAddress(value)
    elif attrtype == ATTR_MAPPED_ADDRESS:
      yield ATTR_MAPPED_ADDRESS, _DecodeAddress(value)
    else:
      yield attrtype, value
def _Encode(msgtype, transaction_id, attrs=()):
  # if DEBUG: print(attrs)
  """Encode message type, transaction id, and attrs into a STUN message."""
  if DEBUG: print(type(transaction_id))
  transaction_id = (transaction_id)
  if len(transaction_id) != 12:
    raise ValueError('transactionid %r must be exactly 12 bytes'
                     % transaction_id)
  attrtext = b''.join(_EncodeAttr(attrtype, attrval)
                     for attrtype, attrval in attrs)
  pkt = (struct.pack('!HHI', msgtype, len(attrtext), MAGIC_COOKIE) +
         transaction_id + 
         attrtext)
  return pkt
def _Decode(text):
  """Decode a STUN message into message type, transaction id, and attrs."""
  if len(text) < 20:
    raise ParseError('packet length %d must be >= 20' % len(text))
  msgtype, length, cookie = struct.unpack('!HHI', text[:8])
  transaction_id = text[8:20]
  attrtext = text[20:]
  if cookie != MAGIC_COOKIE:
    raise ParseError('cookie %r should be %r' % (cookie, MAGIC_COOKIE))
  if length != len(attrtext):
    raise ParseError('packet length field: expected %d, got %d'
                     % (len(attrtext), length))
  attrs = list(_DecodeAttrs(attrtext, transaction_id))
  return msgtype, transaction_id, attrs
def mapAttr(attrs):
  c = 0
  attrlist = [k for k in globals().keys() if k.startswith("ATTR_")]
  #build rev dict
  rev_attrlist = {}
  for attr in attrlist:
    if attr not in rev_attrlist:
      rev_attrlist[globals()[attr]] = attr
  for k, v in attrs:
    try:
      tmp = ("{} => {}".format(rev_attrlist[k], v))
      if DEBUG : print (tmp)
      c+=1
    except KeyError as KE:
      print("KeyError" + repr(KE))
  return c
def INPROC_ATTR_XOR_PEER_ADDRESS(s):
  port, ip = s[:2], s[2:]
  xorkey = struct.pack("!H", (MAGIC_COOKIE >> 0x10))
  port = _Xor(port, xorkey)
  port = struct.unpack("!H", (port))[0]
  xorkey = struct.pack("!I", (MAGIC_COOKIE))
  return('.'.join(map(str,list(_Xor(ip, xorkey))))).encode('utf-8'),port
def PROC_ATTR_XOR_PEER_ADDRESS(ip, port):
  xorkey = struct.pack("!H", (MAGIC_COOKIE >> 0x10))
  port = struct.pack("!H", (port))
  port = _Xor(port, xorkey)
  ip = b''.join(map(lambda i: bytes([int(i)]), ip.decode('utf-8').split('.')))
  xorkey = struct.pack("!I", (MAGIC_COOKIE))
  return port + _Xor(ip, xorkey)
def ServerAddresses(sock, servers=None, extended=False, verbose=False):
  """Return a list of DNS-resolved addresses for a set of STUN servers.
  Args:
    sock: an example socket to look up STUN servers for.  (eg. if it's an IPv6
      socket, we need to use IPv6 STUN servers.)
    servers: a list of STUN server names (defaults to an internal list of
      Google-provided servers).
    extended: if using the default server list and this is true, extend
      the list of servers to include some non-Google servers for extra
      coverage.  (This is important when doing advanced NAT penetration.)
    verbose: log some progress messages to stderr.
  Returns:
    A list of (ip, port) pairs, each of which is suitable for use with
    sock.connect() or sock.sendto().
  Raises:
    DnsError: if none of the server names are valid.
    (DnsError is a subclass of Error)
  """
  
  if extended:
    servers = GOOGLE_SERVER_LIST + EXTRA_SERVER_LIST
  
  out = []
  ex = None
  # TODO(apenwarr): much faster if we do DNS in parallel by fork()ing
  if verbose:
    Log('stun: looking up %d DNS names', len(servers))
  for server in servers:
    server = server.strip()
    if ':' in server:
      host, port = server.split(':', 1)
    else:
      host, port = server, PORT
    try:
      addrs = socket.getaddrinfo(host, port, sock.family, sock.type,
                                 sock.proto, socket.AI_V4MAPPED)
      for i in addrs:
        out.append(i[4])  # host, port only
    except socket.error as e:
      ex = e
  if ex and not out:
    raise DnsError(ex)
  return list(set(out))  # remove duplicates
def Lookup(sock, addrs, timeout, verbose=False):
  """Return a list of our publicly-visible (ipaddr, port) combinations.
  This queries a list of STUN servers.  Some NATs may map the same local
  address to different IP addresses and/or ports depending on what destination
  address you connect to, so this function may return multiple answers.
  For the same reason, this function needs to send/receive STUN packets
  from/to exactly the same socket you intend to use for communication after
  discovering its address.  Thus you should be prepared to receive and
  discard any extraneous STUN response packets on that socket for a while,
  even after this function returns.
  Args:
    sock: the socket to discover the address of.
    addrs: a list of STUN servers, probably from calling ServerAddresses().
    timeout: the maximum number of seconds to wait for a response, in seconds.
    verbose: log some progress messages to stderr.
  Returns:
    A list of (ip, port) pairs, each of which is suitable for use with
    sock.connect() or sock.sendto().  Presumably you will provide one or
    more of these addresses to some other client who wants to talk to you.
  Raises:
    ParseError: if we receive an invalid STUN packet.
    TimeoutError: if no servers respond before the timeout.
    (Both errors are subclasses of Error)
  """
  waiting = set(addrs)
  out = set()
  step = 0
  now = gettime()
  next_time = now
  end_time = now + timeout
  transaction_id = os.urandom(12)
  pkt = _Encode(MSG_BIND_REQUEST, transaction_id)
  while now < end_time:
    if now >= next_time:
      if verbose:
        Log('stun: sending %d requests', len(waiting))
      for addr in waiting:
        sock.sendto(pkt, 0, addr)
      next_time = min(end_time, now + 0.5 * (2 ** step))
      step += 1
    now = gettime()
    r, _, _ = select.select([sock], [], [], max(0, next_time - now))
    if r:
      rpkt, raddr = sock.recvfrom(4096)
      if raddr not in waiting:
        if verbose:
          Log('warning: %r: unexpected response, ignoring.', raddr)
      else:
        rtype, rtransaction_id, rattrs = _Decode(rpkt)
        if rtype != MSG_BIND_SUCCESS:
          if verbose:
            Log('warning: %r: got transaction type %r, expected %r',
                raddr, rtype, MSG_BIND_SUCCESS)
        elif rtransaction_id != transaction_id:
          if verbose:
            Log('warning: %r: got transaction id %r, expected %r',
                raddr, rtransaction_id, transaction_id)
        else:
          for rattr_type, rattr_value in rattrs:
            if rattr_type == ATTR_MAPPED_ADDRESS:
              out.add(rattr_value)
              if raddr in waiting:
                waiting.remove(raddr)
    elif len(waiting) <= len(addrs)/3:
      # 2/3 of the servers ought to be enough
      break
    now = gettime()
  if not out:
    raise TimeoutError('no servers responded')
  return list(out)


def createTCPRelayUpstream(remoteip, remoteport, peerip, peerport, username, password):
  global args
  Log(f"Connect server {remoteip.decode('utf-8')}:{remoteport} ---> {peerip.decode('utf-8')}:{peerport}")
  client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  client.connect((remoteip, remoteport))
  if DEBUG:  print(client)
  transaction_id = os.urandom(12)
  attributes = (ATTR_REQUESTED_TRANSPORT,b'\x06\0\0\0'),
  pkt = _Encode(MSG_ALLOCATE_REQUEST, transaction_id, attributes)
  client.send(pkt)
  resp = (_Decode(client.recv(1024)))
  if resp[0] != MSG_ALLOCATE_SUCCESS:
    Log("Unauthozied. Retrying Authz. Check Wireshark")
  
  # resp = _Decode(binascii.unhexlify("0113004c2112a4421ab43eb31c47a2ad223480d40009001000000401556e617574686f72697a656400150010643233336132313239373863316536610014000d6d79636f6d70616e792e6f7267000000802200044e6f6e658028000468068c67"))
  if 1: #resp[0] == MSG_ALLOCATE_ERROR:
    transaction_id = os.urandom(12)
    attrs = dict((x, y) for x, y in resp[2])
    if DEBUG: print (attrs, repr(attrs[ATTR_ERROR_CODE][2:4]))
    if attrs[ATTR_ERROR_CODE][2:4]==b'\4\1':
      # do authen:
      if DEBUG: print("WTF")
      nonce = attrs[ATTR_NONCE]
      realm = attrs[ATTR_REALM]
      # transaction_id = resp[1]
      # transaction_id = b'\xe7\xa2\xfe\x67\x19\x92\xd6\x56\xee\x63\xb6\xb7'
      if DEBUG: print(nonce, realm)

      # Section 9.2.2 defines the key for the long-term credential mechanism.
      if DEBUG: print((username + b':' + realm +  b':' + password))
      key = hashlib.md5(username + b':' + realm +  b':' + password).digest()
      attributes = ((ATTR_REQUESTED_TRANSPORT,b'\6\0\0\0'),
        (ATTR_USERNAME, username),
        (ATTR_REALM, realm),
        (ATTR_NONCE, nonce),
        (ATTR_MESSAGE_INTEGRITY,b'\0' * 20),
        )

      pkt  = _Encode(MSG_ALLOCATE_REQUEST, transaction_id, attributes) 
      # print(binascii.hexlify(pkt))
      messageintegrity = hmac.new(key, pkt[:-24], hashlib.sha1).digest()
      # print( binascii.hexlify(messageintegrity))
      attributes = ((ATTR_REQUESTED_TRANSPORT,b'\6\0\0\0'),
        (ATTR_USERNAME, username),
        (ATTR_REALM, realm),
        (ATTR_NONCE, nonce),
        (ATTR_MESSAGE_INTEGRITY, messageintegrity),
        )

      pkt = _Encode(MSG_ALLOCATE_REQUEST, transaction_id, attributes) 
      client.send(pkt)
      resp = (_Decode(client.recv(1024)))
      if resp[0] != MSG_ALLOCATE_SUCCESS:
        Log("NO HOPE")
        print(repr(resp))

      attrs = (resp[2])
      mapAttr(attrs)

      PROTO_TCP = b'\0\1'
      attributes = ((ATTR_XOR_PEER_ADDRESS, PROTO_TCP + PROC_ATTR_XOR_PEER_ADDRESS(peerip, 0)),
          (ATTR_USERNAME, username),
          (ATTR_REALM, realm),
          (ATTR_NONCE, nonce),
          (ATTR_MESSAGE_INTEGRITY,b'\0' * 20),
        )
      pkt = _Encode(MSG_CREATEPERMISSION_REQUEST, transaction_id, attributes)
      messageintegrity = hmac.new(key, pkt[:-24], hashlib.sha1).digest()
      attributes = ((ATTR_XOR_PEER_ADDRESS, PROTO_TCP + PROC_ATTR_XOR_PEER_ADDRESS(peerip, 0)),
          (ATTR_USERNAME, username),
          (ATTR_REALM, realm),
          (ATTR_NONCE, nonce),
          (ATTR_MESSAGE_INTEGRITY, messageintegrity),
        )

      pkt = _Encode(MSG_CREATEPERMISSION_REQUEST, transaction_id, attributes)
      client.send(pkt)
      resp = (_Decode(client.recv(1024)))
      if resp[0] != MSG_CREATEPERMISSION_SUCCESS:
        Log("Internal Error, plz check Wireshark 2")
      attrs = (resp[2])
      mapAttr(attrs)

      attributes = ((ATTR_XOR_PEER_ADDRESS, PROTO_TCP + PROC_ATTR_XOR_PEER_ADDRESS(peerip, peerport)),
          (ATTR_USERNAME, username),
          (ATTR_REALM, realm),
          (ATTR_NONCE, nonce),
          (ATTR_MESSAGE_INTEGRITY,b'\0' * 20),
        )
      pkt = _Encode(MSG_CONNECTION_REQUEST, transaction_id, attributes)
      messageintegrity = hmac.new(key, pkt[:-24], hashlib.sha1).digest()
      attributes = ((ATTR_XOR_PEER_ADDRESS, PROTO_TCP + PROC_ATTR_XOR_PEER_ADDRESS(peerip, peerport)),
          (ATTR_USERNAME, username),
          (ATTR_REALM, realm),
          (ATTR_NONCE, nonce),
          (ATTR_MESSAGE_INTEGRITY, messageintegrity),
        )
      pkt = _Encode(MSG_CONNECTION_REQUEST, transaction_id, attributes)
      client.send(pkt)
      resp = (_Decode(client.recv(1024)))
      if resp[0] != MSG_CONNECTION_SUCCESS:
        Log("Internal Error, plz check Wireshark 3")
      attrs = (resp[2])
      mapAttr(attrs)
      _, connectionID = attrs[0]
      connectionID = struct.unpack("!I",connectionID)[0]
      if DEBUG: print(f"ConnectionID: {connectionID}")
      if DEBUG: print("Injecting new connection")

      relayclient = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      relayclient.connect((remoteip, remoteport))

      attributes = ((ATTR_CONNECTION_ID, struct.pack("!I",connectionID)),
        (ATTR_USERNAME, username),
          (ATTR_REALM, realm),
          (ATTR_NONCE, nonce),
          (ATTR_MESSAGE_INTEGRITY, b'\0' * 20),
      )

      pkt = _Encode(MSG_CONNECTIONBIND_REQUEST, transaction_id, attributes)
      messageintegrity = hmac.new(key, pkt[:-24], hashlib.sha1).digest()
      attributes = ((ATTR_CONNECTION_ID, struct.pack("!I",connectionID)),
          (ATTR_USERNAME, username),
            (ATTR_REALM, realm),
            (ATTR_NONCE, nonce),
            (ATTR_MESSAGE_INTEGRITY, messageintegrity),
      )

      pkt = _Encode(MSG_CONNECTIONBIND_REQUEST, transaction_id, attributes)
      relayclient.send(pkt)
      #awwww yishhh
      fin = relayclient.recv(1024, socket.MSG_PEEK)
      resp = (_Decode(fin))
      if DEBUG: print("Fake", len(fin), repr(fin))
      if resp[0] != MSG_CONNECTIONBIND_SUCCESS:
        relayclient.close()
        relayclient = None
        Log("Internal Error, plz check Wireshark 4")

  else:
      

    attrs = (resp[2])
    mapAttr(attrs)

    PROTO_TCP = b'\0\1'
    attributes = ((ATTR_XOR_PEER_ADDRESS, PROTO_TCP + PROC_ATTR_XOR_PEER_ADDRESS(peerip, 0)),)
    pkt = _Encode(MSG_CREATEPERMISSION_REQUEST, transaction_id, attributes)
    client.send(pkt)
    resp = (_Decode(client.recv(1024)))
    if resp[0] != MSG_CREATEPERMISSION_SUCCESS:
      Log("Internal Error, plz check Wireshark 5")
    attrs = (resp[2])
    mapAttr(attrs)

    attributes = ((ATTR_XOR_PEER_ADDRESS, PROTO_TCP + PROC_ATTR_XOR_PEER_ADDRESS(peerip, peerport)),)
    pkt = _Encode(MSG_CONNECTION_REQUEST, transaction_id, attributes)
    client.send(pkt)
    resp = (_Decode(client.recv(1024)))
    if resp[0] != MSG_CONNECTION_SUCCESS:
      Log("Internal Error, plz check Wireshark 6")
    attrs = (resp[2])
    mapAttr(attrs)
    _, connectionID = attrs[0]
    connectionID = struct.unpack("!I",connectionID)[0]
    if DEBUG: print(f"ConnectionID: {connectionID}")
    if DEBUG: print("Injecting new connection")

    relayclient = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    relayclient.connect((remoteip, remoteport))

    attributes = (ATTR_CONNECTION_ID, struct.pack("!I",connectionID)),
    pkt = _Encode(MSG_CONNECTIONBIND_REQUEST, transaction_id, attributes)
    relayclient.send(pkt)
    #awwww yishhh
    fin = relayclient.recv(1024, socket.MSG_PEEK)
    resp = (_Decode(fin))
    if DEBUG: print("Fake", len(fin), repr(fin))
    if resp[0] != MSG_CONNECTIONBIND_SUCCESS:
      relayclient.close()
      relayclient = None
      Log("Internal Error, plz check Wireshark7 ")

  attrs = (resp[2])
  peeklen = len(_Encode(resp[0], resp[1], resp[2][:mapAttr(attrs)]))
  relayclient.recv(peeklen)
  if DEBUG: print ("Real", peeklen)

  # relayclient.send(b'''GET / HTTP/1.1\r\nHost: ifconfig.co\r\nUser-Agent:curl/1.2\r\n\r\n''')
  # print(relayclient.recv(1024))

  if DEBUG: print (relayclient)
  return relayclient, client
def createTCPRelayDownstream(args):
  upstreamconn, _ = createTCPRelayUpstream(args.remoteip, args.remoteport, args.peerip, args.peerport, args.username, args.password)

  downstream = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
  downstream.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, downstream.getsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR) | 1)
  downstream.bind((args.bindip, args.bindport))
  downstream.listen(1) 
  # downstream.setblocking(0) #set to non-blocking mode
  SOCKET_LIST = []
  SOCKET_LIST.append(downstream)
  SOCKET_LIST.append(upstreamconn)
  # print(upstreamconn.recv(1024))
  while 1:
    ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST,[],[], 10)
    for sock in ready_to_read:
      # print("Select", sock)
      if sock == downstream: 
        sockfd, addr = downstream.accept()
        SOCKET_LIST.append(sockfd)
        print ("Client {} connected".format(addr))
      elif len(SOCKET_LIST) > 2:
        try:
          # receiving data from the socket.
          data = sock.recv(NET_MSG_SIZE)
          if data:
            for s in SOCKET_LIST:
              if s!= downstream and s!= sock:
                s.send(data)
                # print(data)
          else: 
            if sock in SOCKET_LIST:
                SOCKET_LIST.remove(sock)
        except:
          continue
    if len(SOCKET_LIST) < 2:
      break

  for sock in SOCKET_LIST: sock.close()


def createUDPRelayDownstream(args):
  username = args.username
  password = args.password
  peerip = args.peerip
  peerport = args.peerport
  Log(f"Connect server via UDP {args.remoteip.decode('utf-8')}:{args.remoteport} ---> {args.peerip.decode('utf-8')}:{args.peerport}")
  client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  # client.setblocking(0) #set to non-blocking mode
  client.connect((args.remoteip, args.remoteport))
  if DEBUG:  print(client)
  transaction_id = os.urandom(12)
  attributes = (ATTR_REQUESTED_TRANSPORT,b'\x11\0\0\0'),
  pkt = _Encode(MSG_ALLOCATE_REQUEST, transaction_id, attributes)
  client.send(pkt)

  resp = (_Decode(client.recv(1024)))
  if resp[0] != MSG_ALLOCATE_SUCCESS:
    Log("Unauthozied. Retrying Authz. Check Wireshark")
  
  attrs = (resp[2])
  # mapAttr(attrs)
  transaction_id = os.urandom(12)
  attrs = dict((x, y) for x, y in resp[2])
  if DEBUG:  print (attrs, repr(attrs[ATTR_ERROR_CODE][2:4]))
  # do authen:
  nonce = attrs[ATTR_NONCE]
  realm = attrs[ATTR_REALM]
  # transaction_id = resp[1]
  # transaction_id = b'\xe7\xa2\xfe\x67\x19\x92\xd6\x56\xee\x63\xb6\xb7'
  if DEBUG: print(nonce, realm)

  # Section 9.2.2 defines the key for the long-term credential mechanism.

  key = hashlib.md5(args.username + b':' + realm +  b':' + args.password).digest()
  attributes = ((ATTR_REQUESTED_TRANSPORT,b'\x11\0\0\0'),
    (ATTR_USERNAME, username),
    (ATTR_REALM, realm),
    (ATTR_NONCE, nonce),
    (ATTR_MESSAGE_INTEGRITY,b'\0' * 20),
    )

  pkt  = _Encode(MSG_ALLOCATE_REQUEST, transaction_id, attributes) 
  # print(binascii.hexlify(pkt))
  messageintegrity = hmac.new(key, pkt[:-24], hashlib.sha1).digest()
  # print( binascii.hexlify(messageintegrity))
  attributes = ((ATTR_REQUESTED_TRANSPORT,b'\x11\0\0\0'),
    (ATTR_USERNAME, username),
    (ATTR_REALM, realm),
    (ATTR_NONCE, nonce),
    (ATTR_MESSAGE_INTEGRITY, messageintegrity),
    )

  pkt = _Encode(MSG_ALLOCATE_REQUEST, transaction_id, attributes) 
  client.send(pkt)
  resp = (_Decode(client.recv(1024)))
  if resp[0] != MSG_ALLOCATE_SUCCESS:
    Log("NO HOPE")
    print(repr(resp))

  attrs = (resp[2])
  mapAttr(attrs)

  PROTO_IPV4 = b'\0\1'
  attributes = ((ATTR_XOR_PEER_ADDRESS, PROTO_IPV4 + PROC_ATTR_XOR_PEER_ADDRESS(peerip, 0)),
      (ATTR_USERNAME, username),
      (ATTR_REALM, realm),
      (ATTR_NONCE, nonce),
      (ATTR_MESSAGE_INTEGRITY,b'\0' * 20),
    )
  pkt = _Encode(MSG_CREATEPERMISSION_REQUEST, transaction_id, attributes)
  messageintegrity = hmac.new(key, pkt[:-24], hashlib.sha1).digest()
  attributes = ((ATTR_XOR_PEER_ADDRESS, PROTO_IPV4 + PROC_ATTR_XOR_PEER_ADDRESS(peerip, 0)),
      (ATTR_USERNAME, username),
      (ATTR_REALM, realm),
      (ATTR_NONCE, nonce),
      (ATTR_MESSAGE_INTEGRITY, messageintegrity),
    )

  pkt = _Encode(MSG_CREATEPERMISSION_REQUEST, transaction_id, attributes)
  client.send(pkt)
  resp = (_Decode(client.recv(1024)))
  if resp[0] != MSG_CREATEPERMISSION_SUCCESS:
    Log("Internal Error, plz check Wireshark 2")
  attrs = (resp[2])
  mapAttr(attrs)
  #---------


  print("START")
  downstream = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
  # downstream.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, downstream.getsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR) | 1)
  downstream.bind((args.bindip, args.bindport))

  # print(upstreamconn.recv(1024))
  addr = None
  while 1:
    ready_to_read,ready_to_write,in_error = select.select([client, downstream],[],[], 10)
    for sock in ready_to_read:
      # print("Select", sock)
      if sock == client: 
        resp = (_Decode(client.recv(1024)))
        if resp[0] != MSG_DATA_INDICATOR:
          Log("Internal Error, plz check Wireshark 3")
        attrs = dict((x, y) for x, y in resp[2])
        if DEBUG: print (attrs, repr(attrs[ATTR_DATA]))
        if DEBUG: print("Got from SRV")
        downstream.sendto(attrs[ATTR_DATA], addr)
        if DEBUG: print("Go for ", addr)
      elif sock == downstream:
        data, addr = downstream.recvfrom(4096)
        if DEBUG: print("Sending to srv", data)
        attributes = ((ATTR_XOR_PEER_ADDRESS, PROTO_IPV4 + PROC_ATTR_XOR_PEER_ADDRESS(peerip, peerport)),
          (ATTR_DATA, data),
          )
        pkt = _Encode(MSG_SEND_INDICATOR, transaction_id, attributes)
        client.send(pkt)
  '''
dig +short -p 9000 google.com @127.0.0.1
  '''
def main():
  """STUN and TURN command-line client."""
  parser = argparse.ArgumentParser(description='Python STUN Client')
  subparser = parser.add_subparsers(help='Specify command')
  lookup = subparser.add_parser('lookup', help = "Lookup")
  lookup.add_argument('-s','--servers', default=",".join(GOOGLE_SERVER_LIST), help="Comma-separated STUN servers to use (default=google.com servers)", metavar='server1,server2')
  lookup.add_argument('-x','--extended', action='store_true', help="Add extra non-Google default STUN servers")
  
  lookup.add_argument('-v','--verbose', action='store_true', help="Print extra information about intermediate results")
  lookup.add_argument('-t','--timeout', default=10.0, type=float, help="STUN query timeout (in seconds) [10.0]")
  lookup.add_argument('-q','--quiet', action='store_true', help="Don't print output except the requested fields")

  replay = subparser.add_parser('tcprelay', help = "Relay Server")
  replay.add_argument('-P', '--protocol', default='TCP', type=str, help="Protocol TCP|UDP (default: TCP)")
  replay.add_argument('--bindip', type=str, default='127.0.0.1', help="Listen IP (default: 127.0.0.1)")
  replay.add_argument('--bindport', default=9000, type=int, help="Listen PORT (default: 9000)")
  replay.add_argument('-u', '--username', type=str, default='', help="Username (default: )")
  replay.add_argument('-p', '--password', type=str, default='',help="Password (default: )")
  
  replay.add_argument('--remoteip', type=str, required=True, help="STUN IP")
  replay.add_argument('--remoteport', default=3478, type=int, help="STUN PORT (default: 3478)")
  replay.add_argument('--ssl', action='store_true', help="STUN SSL (unsupported)")
  replay.add_argument('--peerip', required=True, type=str, help="Default PEER IP")
  replay.add_argument('--peerport', required=True, type=int, help="Default PEER PORT")
  replay.add_argument('-v','--verbose', action='store_true', help="Print extra information about intermediate results")

  args = parser.parse_args()

  #--------
  if 'servers' in args:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    addrs = ServerAddresses(s, args.servers and args.servers.split(',') or None,
                            extended=EXTRA_SERVER_LIST if args.extended else None, verbose=args.verbose)
    if args.verbose:
      Log('servers: %s', addrs)
    myaddrs = Lookup(s, addrs, timeout=float(args.timeout), verbose=args.verbose)
    if args.verbose:
      Log('myaddrs: %s', myaddrs)
    myips, myports = zip(*myaddrs)
    if args.verbose:
      Log('myips: %s', list(set(myips)))
      Log('myports: %s', list(set(myports)))
    for ip in myips:
      print (ip)

  if "bindip" in args:
    args.bindip =  args.bindip.encode('utf-8')
    args.remoteip =  args.remoteip.encode('utf-8')
    args.peerip =  args.peerip.encode('utf-8')
    args.username = args.username.encode('utf-8')
    args.password = args.password.encode('utf-8')
    args.protocol = args.protocol.encode('utf-8')
    if args.protocol == b'TCP':
      createTCPRelayDownstream(args)
    else:
      createUDPRelayDownstream(args)
    
if __name__ == '__main__':
  main()
