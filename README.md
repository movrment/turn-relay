# TURN Relay

TURN Relay

Usage:

```bash
#1
python3 turnrelay.py tcprelay --peerip '1.1.1.1' --peerport 53 -v -u test -p test -P UDP --remoteip  192.168.88.102
#2
dig +short -p 9000 google.com @127.0.0.1
```
